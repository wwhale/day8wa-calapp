(function () {
    angular
        .module("calculator")
        .controller("MainCtrl", MainCtrl);

    function MainCtrl() {
        var self = this;

        self.pageType = {
            HOME: "HOME",
            CALCULATOR: "CALCULATOR"
        };

        self.currentPage = self.pageType.HOME;

        self.isPageOpen = function (page) {
            return self.currentPage == page;
        };

        self.open = function (page) {
            self.currentPage = page;
        }
    }

})();

