# README #

# STEPS for calculator app#

* create server/app.js 
* define express(), bodyParser(), PORT, json

* create app/app.module.js
* define module calculator

* create app/appmain.controller.js
* define angular controller MainCtrl
* create function MainCtrl

* create app/appcalculator.js
* define angular controller CalculatorCtrl
* create function CalculatorCtrl

* create app/app.css
* create css class

* create index.html
* define ng-app, link css, define ng-controller, ng-include, link app/app.module.js, app/appmain.controller.js, app/appcalculator.controller.js

* create views/home.html
* create views/calculator.html

* cd to folder
* run nodemon
* open chrome, goto localhost:3000

* Update to git
* git add .
* git commit -m "<message>"
* git push orign master

# STEPS for setup#

#Setup Bitbucket#
* create respo
* Choose public or private repo access level
* create readme.md

#Prepare project structure#
* create folder 
* mkdir <project name>
* mkdir <project name>/server
* mkdir <project name>/client
* new file: /.gitignore
* new file: /.bowerrc

#Prepare Git#

* (To config)
* git init
* add remote origin , git remote add origin <http git url>. e.g. git remote add origin https://wwhale@bitbucket.org/wwhale/day5wa.git
* Create a git ignore file .gitignore e.g. node_modules

* git remote -v (check on the current directory which remote is configured to)
* To remove the git origin remote ( git remote remove origin)

* (To update)
* git add .
* git commit -m "<message>"
* git push orign master

* git pull origin master
* git clone origin master



#Global Utility installation#

* (node.js)
* cd to the working folder 
* npm install -g nodemon
* npm init
* -- entry point
* -- path from bit bucket

* (express.js)
* cd to the working folder 
* npm install –save express
* npm install
* -- node_modules folder created

* (bower.js)
* .bowerrc file create
* -- "directory": "client/bower_components"
* npm install -g bower
* npm i bower –g
* bower init
* bower install bootstrap font-awesome angular --save

* (body-parser)
* npm install body-parser --save

* (heroku)
* install heroku
* heroku login
* heroku create
* git remote add heroku https://git.heroku.com/murmuring-cove-50493.git
* git remote -v
* git add .
* git commit –m “message”* 
* git push heroku master -u


#To run nodemon#
* cd to folder
* nodemon
* node "filename".js

#to test#
* http://localhost:4000 or https://murmuring-cove-50493.herokuapp.com/



This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact